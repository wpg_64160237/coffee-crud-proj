import type Order from "@/types/Order";
import { defineStore } from "pinia";
import { ref } from "vue";
import { useCoffeeStore } from "./coffee";
const useCoffee = useCoffeeStore();
export const useOrderStore = defineStore("order", () => {
  const order = ref<Order[]>([]);
  const OnClilkMenu = (id: number) => {
    if (order.value.findIndex((item) => item.id == id) === -1) {
      order.value.push({
        id: id,
        name: useCoffee.name(id),
        price: useCoffee.price(id),
        count: 1,
        sum: useCoffee.price(id),
      });
    } else {
      AddMenu(id);
    }
  };
  const DelOrder = (id: number) => {
    const index = order.value.findIndex((item) => item.id == id);
    order.value.splice(index, 1);
  };
  const AddMenu = (id: number) => {
    const index = order.value.findIndex((item) => item.id == id);
    order.value[index].count++;
    order.value[index].sum =
      order.value[index].count * order.value[index].price;
  };
  const DelMenu = (id: number) => {
    const index = order.value.findIndex((item) => item.id == id);
    if (order.value[index].count == 1) {
      DelOrder(id);
    } else {
      order.value[index].count--;
      order.value[index].sum =
        order.value[index].count * order.value[index].price;
    }
  };
  return { OnClilkMenu, order, AddMenu, DelMenu, DelOrder };
});
