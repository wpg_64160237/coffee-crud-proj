export default interface Coffee {
  id: number;
  name: string;
  price: number;
  category: string;
  img: string;
}
